#include <glog/logging.h>
#include <glog/log_severity.h>

#include "ice_map.h"

IceMap* IceMap::m_pInstance = NULL;


int IceMap::Push(std::string id, std::string ice_info)
{
	std::unique_lock<std::mutex> lck(mutex_);

	ICE_MAP_ITOR itor = ice_map_.find(id);
	if (itor != ice_map_.end())
	{
		LOG(ERROR)<<"IceMap::Push failed: "<<id;
		
		return -1;
	}
	else
	{
		ice_map_[id] = ice_info;

		LOG(INFO)<<"IceMap::Push success: "<<id;
		
		return 0;
	}
}


int IceMap::Remove(std::string id)
{
	std::unique_lock<std::mutex> lck(mutex_);

	ICE_MAP_ITOR itor = ice_map_.find(id);
	if (itor != ice_map_.end())
	{
		//xxxx *iq_a = itor->second;
		//delete iq_a;
		
		ice_map_.erase(id);
		
		LOG(INFO)<<"IceMap::Remove success: "<<id;
		
		return 0;
	}
	else
	{
		LOG(ERROR)<<"IceMap::Remove failed: "<<id;
			
		return -1;
	}
}

IceMap* IceMap::Instance()  
{  
	if ( m_pInstance == NULL )
	{
		m_pInstance = new IceMap();
	}

	return m_pInstance;  
}

