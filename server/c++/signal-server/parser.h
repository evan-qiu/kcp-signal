#ifndef __PARSER_H_
#define __PARSER_H_

#include <iostream>

#include "event.h"

class Parser
{
public: 
	Parser();
	~Parser();
	
	static  event_type_e ParseEvent(const char *buffer, std::string &id, std::string &peer_id, int &type, std::string &content);

private:
};

#endif

