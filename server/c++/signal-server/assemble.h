#ifndef __ASSEMBLE_H_
#define __ASSEMBLE_H_

#include "jsoncpp/json.h"
#include "client_map.h"

class Assembler
{
public: 
	Assembler() {}
	~Assembler() {}
	
	static int AssembleReqConnMsg(std::string &ice_info, std::string &id, std::string &json_str);
	static int AssembleRespConnMsg(std::string &ice_info, std::string &json_str);
	static int AssembleListDevMsg(ID_LIST &id_list, std::string &json_str);

private:
};

#endif

