#include <glog/logging.h>
#include <glog/log_severity.h>

#include "jsoncpp/json.h"

#include "assemble.h"
#include "event.h"

int Assembler::AssembleReqConnMsg(std::string &ice_info, std::string &id, std::string &json_str)
{
	Json::Value root;
	Json::Value eventType;
	Json::Value deviceId;

	root["event"] = Json::Value(EVENT_TYPE_CONN_REQ);;
	root["data"] = Json::Value(ice_info);
	root["peer_id"] = Json::Value(id);
	
	Json::FastWriter fast_writer;
	json_str = fast_writer.write(root);

	if (json_str.c_str()[json_str.length() - 1] == '\n')
	{
		LOG(INFO)<<"AssembleReqConnMsg----------------------------";
		std::string str(json_str, 0, json_str.length() - 1);
		json_str = str;
	}
	
	LOG(INFO)<<"AssembleReqConnMsg: "<<json_str;
	
	return 0;
}

int Assembler::AssembleRespConnMsg(std::string &ice_info, std::string &json_str)
{
	Json::Value root;
	Json::Value eventType;
	Json::Value deviceId;

	root["event"] = Json::Value(EVENT_TYPE_CONN_RESP);;
	root["data"] = Json::Value(ice_info);
	
	Json::FastWriter fast_writer;
	json_str = fast_writer.write(root);

	if (json_str.c_str()[json_str.length() - 1] == '\n')
	{
		LOG(INFO)<<"AssembleRespConnMsg----------------------------";
		std::string str(json_str, 0, json_str.length() - 1);
		json_str = str;
	}

	LOG(INFO)<<"AssembleRespConnMsg: "<<json_str;
	
	return 0;
}

int Assembler::AssembleListDevMsg(ID_LIST &id_list, std::string &json_str)
{
	Json::Value root;
	Json::Value eventType;
	Json::Value deviceId;

	root["event"] = Json::Value(EVENT_TYPE_LIST_DEVICE);
	//root["ice"] = Json::Value(ice_info);

	std::string id_str;
	int i = 0;
	int size = id_list.size();

	ID_LIST_ITOR itor = id_list.begin();
	for (; itor != id_list.end(); itor++)
	{
		LOG(INFO)<<"AssembleListDevMsg, id: "<<*itor;
		
		id_str += (*itor);

		if (i != size - 1)
		{
			id_str += ",";
		}

		i++;
	}

	LOG(INFO)<<"AssembleListDevMsg: "<<id_str<<", len: "<<id_str.length();

	root["data"] = Json::Value(id_str);
	
	Json::FastWriter fast_writer;
	json_str = fast_writer.write(root);

	if (json_str.c_str()[json_str.length() - 1] == '\n')
	{
		std::string str(json_str, 0, json_str.length() - 1);
		json_str = str;
	}

	LOG(INFO)<<"AssembleListDevMsg: "<<json_str<<", len: "<<json_str.length();
	
	return 0;
}

