#include <string.h>
#include "request_type.h"

SignalCmd::SignalCmd(int type) :MessageRequest(MessageDeliver::MSG_TYPE_SIGNAL)
{
	this->ExtType(type);
	priv_ = NULL;
	memset(payload_, 0, sizeof(payload_));
	size_ = 0;
}

SignalCmd::~SignalCmd()
{

}

