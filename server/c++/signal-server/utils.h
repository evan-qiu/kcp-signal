#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void itimeofday(long *sec, long *usec);
long long iclock64(void);
uint32_t iclock();
void isleep(uint32_t millisecond);

#ifdef __cplusplus
}
#endif

#endif
