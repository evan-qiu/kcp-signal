#ifndef __REQUEST_TYPE_H_
#define __REQUEST_TYPE_H_

#include "dispatch.h"

///////////////////only for test
class SignalCmd : public MessageRequest
{
public:
	SignalCmd(int type);
	~SignalCmd(void);

	virtual bool DelayRelease() {return true;}

	void Destroy() {delete this;}

public:
	void *priv_;
	uint8_t payload_[1500];
	int size_;
};

class TestDeliver: public MessageDeliver 
{	 
public:
	TestDeliver(int type);

	int Execute(void *args);
	int process();
	
private:
	static void *DoWork(void *arg);

private:
	void *data_;
}; 

#endif

