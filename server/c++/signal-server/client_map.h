#ifndef __CLIENT_MAP_H_
#define __CLIENT_MAP_H_

#include <map>
#include <list>
#include <mutex>

class ClientInfo
{
public:
	ClientInfo() :client_(NULL), type_(-1) {}
	~ClientInfo() {}
	
	void operator=(ClientInfo &key);

public:	
	void *client_;
	int type_;
	std::string ice_info_;
};

typedef std::map<std::string, ClientInfo> CLIENT_MAP;
typedef std::map<std::string, ClientInfo>::iterator CLIENT_MAP_ITOR;

typedef std::list<std::string> ID_LIST;
typedef std::list<std::string>::iterator ID_LIST_ITOR;

class ClientMap
{
public:
	ClientMap() {}
	~ClientMap() {}

	int Push(std::string &id, ClientInfo &client);
	int UpdateICE(std::string &id, std::string &ice_info);
	int Remove(std::string &id);

	int GetAgent(std::string &id, ClientInfo &client);

	int GetIDList(ID_LIST &id_list);

	static ClientMap* Instance();

private:
	CLIENT_MAP client_map_;
	static ClientMap * m_pInstance;
	std::mutex mutex_;
	ID_LIST id_list_;
};

#endif

