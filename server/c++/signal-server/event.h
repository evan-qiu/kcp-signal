#ifndef __EVENT_H_
#define __EVENT_H_

enum event_type_e
{
	EVENT_TYPE_INVALID = -1,

	EVENT_TYPE_HEART_BEAT = 0, //terminal-->server, device-->server
	
	EVENT_TYPE_CONN_REQ, //terminal-->server-->device
	
	EVENT_TYPE_CONN_RESP, //device-->server-->terminal
	
	EVENT_TYPE_QUERY_DEVICE, //terminal-->server-->device
	
	EVENT_TYPE_LIST_DEVICE, //server-->terminal
	
	EVENT_TYPE_LAST
};

#endif

