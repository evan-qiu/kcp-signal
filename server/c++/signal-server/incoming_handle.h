#ifndef __INCOMING_HANDLE_H_
#define __INCOMING_HANDLE_H_

#include "request_type.h"

class IncomingHandler : public MessageDeliver
{
public:
	IncomingHandler(int type);
	~IncomingHandler();

	virtual int Execute(void *args);

	int HandleMsg(SignalCmd *msg);

private:
	void HandleHB(std::string &id, int type, void *data);
	void HandleConnReq(std::string &id, std::string &peer_id, std::string &ice_info, void *data);
	void HandleConnResp(std::string &id, std::string &peer_id, std::string &ice_info, void *data);
	void HandleQueryDevice(std::string &id, void *data);
};

#endif

