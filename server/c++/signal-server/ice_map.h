#ifndef __ICE_MAP_H_
#define __ICE_MAP_H_

#include <map>
#include <mutex>

typedef std::map<std::string, std::string> ICE_MAP;
typedef std::map<std::string, std::string>::iterator ICE_MAP_ITOR;

class IceMap
{
public:
	IceMap() {}
	~IceMap() {}

	int Push(std::string id, std::string ice_info);
	int Remove(std::string id);

	static IceMap* Instance();

private:
	ICE_MAP ice_map_;
	static IceMap * m_pInstance;
	std::mutex mutex_;
};

#endif

