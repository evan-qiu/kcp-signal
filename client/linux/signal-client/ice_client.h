#ifndef _ICE_CLIENT_H_
#define _ICE_CLIENT_H_

typedef struct 
{
	char ip[32];
	unsigned short port;
	int action;
	char *peer_ice;
	char *peer_id;
}ice_config;

void StartICE(ice_config *cfg);

char *GetICECandidate();

int GetICEWritePipe();


#endif

