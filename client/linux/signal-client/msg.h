#ifndef __MSG_H_
#define __MSG_H_

#include "queue.h"

typedef struct 
{
	int seq_;
	char *msg_;
}Command;

typedef struct 
{
	Command cmd_;
	struct list_head list_;
}MsgNode;

void InitSendQueue();
void PushBack2SndQueue(MsgNode *msg);
int GetSndPipeFd();

void InitRecvQueue();
void PushBack2RcvQueue(MsgNode *msg);

#endif

