#include <stdio.h>
#include <stdlib.h>

#include "queue.h"
#include "msg.h"
#include "custom.h"
#include "kcp_client.h"

extern int device_type;

int main(int argc, char *argv[])
{
	if(argc != 4)
	{
		printf("Please input server, port and work-mode(0/1)!\n");
		printf("work-mode 0: waiting for connect\n");
		printf("work-mode 1: want to connect someone\n");
		printf("Example: ./client 192.168.0.100 7799 1\n");
		return -1;
	}
	
	printf("This is kcp client\n");

	device_type = atoi(argv[3]);

	InitCommJsonMsg();

	InitSendQueue();

	InitRecvQueue();

	KCPInit(argv[1], atoi(argv[2]));

	KCPLoop();

	return 0;	
}

