#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdint.h>

void itimeofday(long *sec, long *usec);
long long iclock64(void);
uint32_t iclock();
void isleep(uint32_t millisecond);

#endif

