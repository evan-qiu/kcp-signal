#include <stdlib.h>
#include <stdio.h>

#include "queue.h"

void QueueCreat(struct list_head *list)
{
    INIT_LIST_HEAD(list);
}

void Enqueue(struct list_head *node, struct list_head *head)
{
    list_add_tail(node, head);
}

void Dequeue(struct list_head *head)
{
    struct list_head *list = head->next; 
    
    list_del(head->next);
    
    INIT_LIST_HEAD(list);
}

int QueueSize(struct list_head *head)
{ 
    struct list_head *pos;
    int size = 0;
    
    if (head == NULL) 
	{
        return -1;
    }
    
    list_for_each(pos, head) {
        size++;
    }

    return size;
}

bool IsQueueEmpty(struct list_head *head)
{
    return list_empty(head);
}

////test
///////////////////////////////////////////////////////////////////////////////
typedef struct person
{
    int age;
    struct list_head list;
}person;

int testQ()
{
    int i;
    int num =5;
    struct person *p;
    struct person head;
    struct person *pos,*n;
    
    QueueCreat(&head.list);
    
    p = (struct person *)malloc(sizeof(struct person )*num);
    
    for (i = 0;i < num;i++) 
	{
        p->age = i*10;
        Enqueue(&p->list, &head.list);
        p++;
    }
	
    printf("original==========>\n");
	
    list_for_each_entry_safe(pos,n,&head.list,list)
	{
        printf("age = %d\n",pos->age);
    }
	
    printf("size = %d\n", QueueSize(&head.list));
	
    struct person test;
    test.age = 100;


	person *p1 = GET_QUEUE_HEAD(struct person, &head.list, list);
	printf("1 Dequeue %d\n", p1->age);
 
    Dequeue(&head.list);

	p1 = GET_QUEUE_HEAD(struct person, &head.list, list);
	printf("2 Dequeue %d\n", p1->age);

    Dequeue(&head.list);
    printf("Enqueue %d\n",test.age);
	
    Enqueue(&test.list,&head.list);

    printf("current==========>\n");
    list_for_each_entry_safe(pos,n,&head.list,list) {
        printf("age = %d\n",pos->age);
    }
	
    printf("size = %d\n",QueueSize(&head.list));
    printf("all member Dequeue\n");
    list_for_each_entry_safe(pos,n,&head.list,list) {
        Dequeue(&head.list);
    }
	
    printf("size = %d\n",QueueSize(&head.list));
    if (IsQueueEmpty(&head.list)) {
        printf("IsQueueEmpty\n");
    }
    
    return 0;
}

