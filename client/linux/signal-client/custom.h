#ifndef __CUSTOM_H_
#define __CUSTOM_H_

typedef enum
{
	EVENT_TYPE_INVALID = -1,

	EVENT_TYPE_HEART_BEAT = 0, //terminal-->server, device-->server
	
	EVENT_TYPE_CONN_REQ, //terminal-->server-->device
	
	EVENT_TYPE_CONN_RESP, //device-->server-->terminal
	
	EVENT_TYPE_QUERY_DEVICE, //terminal-->server-->device
	
	EVENT_TYPE_LIST_DEVICE, //server-->terminal
	
	EVENT_TYPE_LAST
}event_type_e;

void InitCommJsonMsg();

char *GetRespMsg();
char *GetPeriodMsg();
char *GetID();

int ParseEvent(char *json_data, 
					char *data, 
					int max_data_size,
					char *peer_id,
					int max_id_size);

char *CreateEvent(event_type_e eventId, char *id, char *peer_id, char *msg);

void QueryDevList();

void HandleConnResp(char *peer_ice);

void HandleConnReq(char *peer_ice, char *peer_id);

void HandleDevList(char *dev_list);

void ReqConn(char *peer_id, char *ice);

void RespConn(char *peer_id, char *ice);


//for test
int StartTestSnd();

#endif

