#ifndef __QUEUE_H_
#define __QUEUE_H_

#include "list.h"

#define GET_QUEUE_HEAD(pos, head, member)  list_first_entry(head, pos, member)

void QueueCreat(struct list_head *list);
void Enqueue(struct list_head *node, struct list_head *head);
void Dequeue(struct list_head *entry);
int QueueSize(struct list_head *head);
bool IsQueueEmpty(struct list_head *head);


int testQ();

#endif

