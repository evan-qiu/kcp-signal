#ifndef __KCP_CLIENT_H_
#define __KCP_CLIENT_H_

#include "ikcp.h"

void KCPInit(char *srv_ip, unsigned short srv_port);
void KCPLoop();

#endif

